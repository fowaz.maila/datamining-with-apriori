/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apriori;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author Fowaz PC
 */
public class Apriori {
    
    public Set<Set<Item>> nonFrequent;
    public Set<Set<Item>> frequent;
    public HashMap<Set<Item>, Integer> joinResult;
    public HashMap<Set<Item>, Integer> joinResultHistory;
    public ArrayList<Transaction> transactions;
    public int minSupportCount;
    public HashMap<String, Double> rules;
    
    public Apriori (int minSupportCount, ArrayList<Transaction> transactions){
        nonFrequent = new HashSet<Set<Item>>();
        frequent = new HashSet<Set<Item>>();
        joinResult = new HashMap<Set<Item>, Integer>();
        joinResultHistory = new HashMap<Set<Item>, Integer>();
        this.transactions=transactions;
        this.minSupportCount=minSupportCount;
        rules=new HashMap<>();
    }
    
    public void calculateJoin(int n){
        if(n==1){
            for(Item i : Item.values()){
                Set<Item> s = new HashSet<Item>();
                s.add(i);
                
                int count=0;
                
                for (Transaction t : transactions)
                    if(t.containsItem(i))
                        count++;
                
                joinResult.put(s, count);
            }       
        } else {
            for(Set<Item> s1 : frequent)
                for(Set<Item> s2 : frequent){
                    //ensure not to join set with it self
                    if(!s1.toString().equals(s2.toString())){
                        Set<Item> s = new HashSet<Item>();
                        s.addAll(s1);
                        s.addAll(s2);
                        //ensure join result have the same number of elements as n (or k)
                        if(s.size()!=n)
                            continue;
                        
                        if(!checkSubsetsUnfrequent(s)){
                            //calculate support
                            int count=0;
                
                            for (Transaction t : transactions)
                                if(t.contains(s))
                                    count++;
                        
                            joinResult.put(s, count);
                        }
                    }
                }
        }
    }
    
    public void calculateFrequent(){
        frequent.clear();
        
        for(Map.Entry<Set<Item>, Integer> set : joinResult.entrySet()){
            if(set.getValue()<this.minSupportCount){
                nonFrequent.add(set.getKey());
            } else {
                frequent.add(set.getKey());
            }
            
            joinResultHistory.put(set.getKey(), set.getValue());
        }
        joinResult.clear();
    }
    
    public Set<Set<Item>> findSubsets(Set<Item> s){
        Set<Set<Item>> res = new HashSet<Set<Item>>();
        int n = s.size();
        var x = s.toArray();
        
        //it should start from 0 to include empty subset, but here it's unconvienient
        for (int i = 1; i < (1 << n); i++) {
            
            // Loop through all elements of the input array
            Set<Item> temp = new HashSet<Item>();
            for (int j = 0; j < n; j++) {
 
                // Check if the jth bit is set
                // in the current subset
                if ((i & (1 << j)) != 0) {
 
                    // If the jth bit is set,
                    // add the jth
                    // element to the subset
                    temp.add((Item)x[j]);
                }
            }
 
            res.add(temp);
        }
        
        return res;
    }
    
    public Set<Set<Item>> findSubsetsWithoutSameSet(Set<Item> s){
        Set<Set<Item>> res = new HashSet<Set<Item>>();
        int n = s.size();
        var x = s.toArray();
        
        //it should start from 0 to include empty subset, but here it's unconvienient
        for (int i = 1; i < (1 << n)-1; i++) {
            
            // Loop through all elements
            // of the input array
            Set<Item> temp = new HashSet<Item>();
            for (int j = 0; j < n; j++) {
 
                // Check if the jth bit is set
                // in the current subset
                if ((i & (1 << j)) != 0) {
 
                    // If the jth bit is set,
                    // add the jth
                    // element to the subset
                    temp.add((Item)x[j]);
                }
            }
 
            res.add(temp);
        }
        
        return res;
    }
    
    public boolean checkSubsetsUnfrequent(Set<Item> s){
        Set<Set<Item>> subsets = findSubsets(s);
        
        for(Set<Item> temp : subsets)
            if(nonFrequent.contains(temp))
                return true;
        
        return false;
    }
    
    public Set<Set<Item>> performAlgorithm(){
        Set<Set<Item>> res = new HashSet<Set<Item>>();
        int k=1;
        while(true){
            calculateJoin(k++);
            //dont write frequent=ap.frequent because this is another reference to the same object
            //this variable stores the previous frequent set because the loop stops when frequent.size=0
            //and at this point we need the previous frequent
            res=new HashSet<Set<Item>>(frequent);
            calculateFrequent();
            //stop condition: there's no more frequent set with k items
            if(frequent.size()==0)
                break; 
        }
        return res;
    }
    
    public void extractRules(double minConfidence, boolean withLabels){
        Set<Set<Item>> res = performAlgorithm();
        for(Set<Item> s: res){
            //generate all non empty & not the same set  subsets
            Set<Set<Item>> subsets = findSubsetsWithoutSameSet(s);

            for(Set<Item> subset : subsets){
                //copy the frequent set
                Set<Item> temp = new HashSet<>(s);
                //calculate frequent set - subset
                temp.removeAll(subset);
                //get support count for frequent set
                double freqSetSupport = Double.valueOf(joinResultHistory.get(s));
                //get support count for subset
                double subsetSupport = Double.valueOf(joinResultHistory.get(subset));
                //calc the confidence
                double confidence = freqSetSupport/subsetSupport*100;
                if(confidence>=minConfidence){
                    if(withLabels){
                        
                        String key="[";
                        for(Item i : subset)
                            key+=i.labelEn+", ";
                        key=key.substring(0, key.length()-2)+"]";
                        
                        String value="[";
                        for(Item i : temp)
                            value+=i.labelEn+", ";
                        value=value.substring(0, value.length()-2)+"]";
                        
                        rules.put(key+" => "+value, confidence);
                    }
                    else
                        rules.put(subset.toString()+" => "+temp.toString(), confidence);
                }
    
            }
        }

    }
    
}
