/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apriori;

import java.util.ArrayList;
import java.util.Set;

public class Transaction {
    public ArrayList<Item> items;

    public Transaction() {
        this.items = new ArrayList<>();
    }
    
    public void addItem(Item ...items){
        for (Item x : items)
        this.items.add(x);
    }
    
    public boolean containsItem(Item i){
        for(Item x : items)
            if(x==i)
                return true;
        return false;
    }
    
    public boolean contains(Set<Item> s){
        if(items.containsAll(s))
            return true;
        return false;
    }
    
    public String toString(){
        String s="";
        for(Item i : items)
            s+=i+", ";
        //trim the last ", "
        return s.substring(0, s.length()-2);
    }
    
}
