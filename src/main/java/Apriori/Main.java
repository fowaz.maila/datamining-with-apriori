/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apriori;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Fowaz PC
 */
public class Main {

    public static void main(String[] args) {
        Transaction t1 = new Transaction();
        t1.addItem(Item.I1, Item.I2, Item.I5);

        Transaction t2 = new Transaction();
        t2.addItem(Item.I2, Item.I4);

        Transaction t3 = new Transaction();
        t3.addItem(Item.I2, Item.I3);

        Transaction t4 = new Transaction();
        t4.addItem(Item.I1, Item.I2, Item.I4);

        Transaction t5 = new Transaction();
        t5.addItem(Item.I1, Item.I3);

        Transaction t6 = new Transaction();
        t6.addItem(Item.I2, Item.I3);

        Transaction t7 = new Transaction();
        t7.addItem(Item.I1, Item.I3);

        Transaction t8 = new Transaction();
        t8.addItem(Item.I1, Item.I2, Item.I3, Item.I5);

        Transaction t9 = new Transaction();
        t9.addItem(Item.I1, Item.I2, Item.I3);

        ArrayList<Transaction> transactions = new ArrayList();
        transactions.add(t1);
        transactions.add(t2);
        transactions.add(t3);
        transactions.add(t4);
        transactions.add(t5);
        transactions.add(t6);
        transactions.add(t7);
        transactions.add(t8);
        transactions.add(t9);

        //build the GUI
        JFrame f = new JFrame("Apriori");
        JButton b = new JButton("Calculate Strong Association Rules");
        b.setBounds(550, 10, 300, 30);
        f.add(b);

        JLabel jbl = new JLabel("Min Support Count: ");
        jbl.setBounds(30, 10, 200, 30);
        f.add(jbl);
        JTextField minSupportCount = new JTextField();
        minSupportCount.setBounds(150, 10, 100, 30);
        f.add(minSupportCount);

        JLabel jb2 = new JLabel("Min Confidence: ");
        jb2.setBounds(290, 10, 200, 30);
        f.add(jb2);
        JTextField minConfidence = new JTextField();
        minConfidence.setBounds(400, 10, 100, 30);
        f.add(minConfidence);

        JTextArea result = new JTextArea();
        result.setBounds(30, 50, 820, 500);
        result.setLineWrap(true);
        Font font = new Font("Times New Roman", Font.BOLD, 13);
        result.setFont(font);
        result.setForeground(Color.BLACK);
        result.setEnabled(false);
        result.setDisabledTextColor(Color.BLACK);
        f.add(result);

        f.setSize(900, 600);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //remove previuos results
                result.setText("");

                try {
                    int minSupport = Integer.valueOf(minSupportCount.getText());
                    double minConf = Double.valueOf(minConfidence.getText());
                    
                    if(minConf>100 || minConf<0)
                        throw new Exception("Minimum Confidence should be in range [0, 100]");
                    
                    if(minSupport<=0)
                        throw new Exception("Minimum Support should grater than 0");

                    //implement Apriori
                    Apriori ap = new Apriori(minSupport, transactions);

                    ap.extractRules(minConf, false);

                    for (Map.Entry<String, Double> rule : ap.rules.entrySet()) {
                        result.append("\n  " + rule.getKey() + ": " + String.format("%.2f", rule.getValue()));
                    }

                } catch (NumberFormatException nex){
                    result.setText("Minimun Support & Minimun Confidence should be numbers only...");
                } catch (Exception ex) {
                    result.setText(ex.getMessage());
                }

            }
        });

    }

}
