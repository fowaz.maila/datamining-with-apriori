/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apriori;

/**
 *
 * @author Fowaz PC
 */
public enum Item {    
    
    I1(1, "ساندويش شاورما دجاج", "Chicken Shawrma Sandwich", ""),
    I2(2, "ساندويش شاورما دجاج دبل", "Chicken Shawrma Sandwich Doubled", ""),
    I3(3, "وجبة شاورما دجاج", "Chicken Shawrma Meal", ""),
    I4(4, "وجبة شاورما دجاج دبل", "Chicken Shawrma Meal Doubled", ""),
    I5(5, "1/2 كغ شاورما دجاج", "1/2Kg Chicken Shawrma", ""),
    I6(6, "وجبة شاورما دجاج 250 غرام", "250g Chicken Shawrma", ""),
    I7(7, "ساندويش شاورما لحم", "Veal Shawrma Sandwich", ""),
    I8(8, "ساندويش شاورما لحم دبل", "Veal Shawrma Sandwich Doubled", ""),
    I9(9, "وجبة شاورما لحم", "Veal Shawrma Meal", ""),
    I10(10, "وجبة شاورما لحم دبل", "Veal Shawrma Meal Doubled", ""),
    I11(11, "1/2 كغ شاورما لحم", "1/2Kg Veal Shawrma", ""),
    I12(12, "ساندويش بطاطا مقلية", "French Fries Sandwich", ""),
    I13(13, "ساندويش فاهيتا", "Fajita Sandwitch", ""),
    I14(14, "ساندويش كريسبي", "Crispy Sandwich", ""),
    I15(15, "ساندويش شيش", "Shiesh Sandwich", ""),
    I16(16, "ساندويش اسكالوب", "Escalope Sandwich", ""),
    I17(17, "ساندويش زنجر", "Zinger Sandwich", ""),
    I18(18, "وجبة كريسبي حجم صغير", "Mini Crispy Meal", ""),
    I19(19, "وجبة اسكالوب حجم صغير", "Mini Escalope Meal", ""),
    I20(20, "وجبة شيش حجم صغير", "Mini Shiesh Meal", ""),
    I21(21, "وجبة فاهيتا", "Fajita Meal", ""),
    I22(22, "برغر دجاج", "Chicken Burger", ""),
    I23(23, "برغر لحم", "Veal Burger", ""),
    I24(24, "تشيز برغر", "Cheese Burger", ""),
    I25(25, "بيغ ماك برغر", "BigMac Burger", ""),
    I26(26, "سلطة سيزر", "Caesar Salad", ""),
    I27(27, "سلطة كول سلو", "Coleslaw Salad", ""),
    I28(28, "سلطة ذرة", "Corn Salad", ""),
    I29(29, "بيتزا مرغريتا حجم وسط", "Medium Margerita Pizza", ""),
    I30(30, "بيتزا خضار حجم وسط", "Medium Vegetable Pizza", ""),
    I31(31, "بيتزا فصول أربعة حجم وسط", "Medium Four Season Pizza", ""),
    I32(32, "بيتزا دجاج حجم وسط", "Medium Chicken Pizza", ""),
    I33(33, "بيتزا مرغريتا حجم كبير", "Large Margerita Pizza", ""),
    I34(34, "بيتزا خضار حجم كبير", "Large Vegetable Pizza", ""),
    I35(35, "بيتزا فصول أربعة حجم كبير", "Large Four Season Pizza", ""),
    I36(36, "بيتزا دجاج حجم كبير", "Large Chicken Pizza", ""),
    I37(37, "علبة كريم ثوم صغيرة", "Mini Garlic Cream", ""),
    I38(38, "علبة كريم ثوم كبيرة", "Large Garlic Cream", ""),
    I39(39, "علبة بطاطا مقلية حجم وسط", "Medium French Fries", ""),
    I40(40, "علبة بطاطا مقلية حجم كبير", "Large French Fries", ""),
    I41(41, "كولا حجم صغير", "Small Cola", ""),
    I42(42, "كولا حجم وسط", "Medium Cola", ""),
    I43(43, "كولا حجم كبير", "Large Cola", ""),
    I44(44, "كولا حجم وسط", "Medium Cola", ""),
    I45(45, "كولا حجم وسط", "Medium Cola", ""),
    I46(46, "كولا حجم وسط", "Medium Cola", ""),
    I47(47, "كولا حجم وسط", "Medium Cola", ""),
    I48(48, "كولا حجم وسط", "Medium Cola", ""),
    I49(49, "كولا حجم وسط", "Medium Cola", ""),
    I50(50, "كولا حجم وسط", "Medium Cola", ""),
    I51(51, "كولا حجم وسط", "Medium Cola", ""),
    I52(52, "كولا حجم وسط", "Medium Cola", ""),
    I53(53, "كولا حجم وسط", "Medium Cola", ""),
    I54(54, "كولا حجم وسط", "Medium Cola", ""),
    I55(55, "كولا حجم وسط", "Medium Cola", ""),
    I56(56, "كولا حجم وسط", "Medium Cola", ""),
    I57(57, "كولا حجم وسط", "Medium Cola", ""),
    I58(58, "كولا حجم وسط", "Medium Cola", ""),
    I59(59, "كولا حجم وسط", "Medium Cola", ""),
    I60(60, "كولا حجم وسط", "Medium Cola", ""),
    I61(61, "كولا حجم وسط", "Medium Cola", ""),
    I62(62, "كولا حجم وسط", "Medium Cola", ""),
    I63(63, "كولا حجم وسط", "Medium Cola", ""),
    I64(64, "كولا حجم وسط", "Medium Cola", ""),
    I65(65, "كولا حجم وسط", "Medium Cola", ""),
    I66(66, "كولا حجم وسط", "Medium Cola", ""),
    I67(67, "كولا حجم وسط", "Medium Cola", ""),
    I68(68, "كولا حجم وسط", "Medium Cola", ""),
    I69(69, "كولا حجم وسط", "Medium Cola", ""),
    I70(70, "كولا حجم وسط", "Medium Cola", ""),
    I71(71, "كولا حجم وسط", "Medium Cola", ""),
    I72(72, "كولا حجم وسط", "Medium Cola", ""),
    I73(73, "كولا حجم وسط", "Medium Cola", ""),
    I74(74, "كولا حجم وسط", "Medium Cola", ""),
    I75(75, "كولا حجم وسط", "Medium Cola", ""),
    I76(76, "كولا حجم وسط", "Medium Cola", ""),
    I77(77, "كولا حجم وسط", "Medium Cola", ""),
    I78(78, "كولا حجم وسط", "Medium Cola", ""),
    I79(79, "كولا حجم وسط", "Medium Cola", ""),
    I80(80, "كولا حجم وسط", "Medium Cola", ""),
    I81(81, "كولا حجم وسط", "Medium Cola", ""),
    I82(82, "كولا حجم وسط", "Medium Cola", ""),
    I83(83, "كولا حجم وسط", "Medium Cola", ""),
    I84(84, "كولا حجم وسط", "Medium Cola", ""),
    I85(85, "كولا حجم وسط", "Medium Cola", ""),
    I86(86, "كولا حجم وسط", "Medium Cola", ""),
    I87(87, "كولا حجم وسط", "Medium Cola", ""),
    I88(88, "كولا حجم وسط", "Medium Cola", ""),
    I89(89, "كولا حجم وسط", "Medium Cola", ""),
    I90(90, "كولا حجم وسط", "Medium Cola", ""),
    I91(91, "كولا حجم وسط", "Medium Cola", "");

    public int id;
    public String label;
    public String labelEn;
    public String stringID;
    
    private Item(int id, String label, String labelEn, String sid){
        this.id=id;
        this.label=label;
        this.labelEn=labelEn;
        this.stringID=sid;
    }
    
    public static Item findByID(int id){
        for (Item i : Item.values())
            if(i.id==id)
                return i;
        return null;
    }
    
    public static Item findByStringID(String sid){
        for (Item i : Item.values())
            if(i.stringID.equals(sid))
                return i;
        return null;
    }
    
}
