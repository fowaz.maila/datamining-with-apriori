/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apriori;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DataSetMain {

    public static void main(String[] args) throws FileNotFoundException {
        String temp;
        Scanner in = new Scanner(new File("pizza.csv"));

        //read the type of pizza & assign them to Enumneration
        while (in.hasNext()) {
            temp = in.nextLine();
            int firstComma = temp.indexOf(',');
            int secondComma = temp.indexOf(',', firstComma + 1);

            //System.out.println(temp);
            int id = Integer.valueOf(temp.substring(0, firstComma));
            String sid = temp.substring(firstComma + 1, secondComma);
            String label = temp.substring(secondComma + 1);

            Item i = Item.findByID(id);
            i.labelEn = label;
            i.stringID = sid;
        }

        //build the GUI
        JFrame f = new JFrame("Apriori");
        JButton b = new JButton("Calculate Strong Association Rules");
        b.setBounds(550, 10, 300, 30);
        f.add(b);

        JLabel jbl = new JLabel("Min Support Count: ");
        jbl.setBounds(30, 10, 200, 30);
        f.add(jbl);
        JTextField minSupportCount = new JTextField();
        minSupportCount.setBounds(150, 10, 100, 30);
        f.add(minSupportCount);

        JLabel jb2 = new JLabel("Min Confidence: ");
        jb2.setBounds(290, 10, 200, 30);
        f.add(jb2);
        JTextField minConfidence = new JTextField();
        minConfidence.setBounds(400, 10, 100, 30);
        f.add(minConfidence);

        JTextArea result = new JTextArea();
        result.setBounds(30, 50, 820, 500);
        result.setLineWrap(true);
        Font font = new Font("Times New Roman", Font.BOLD, 13);
        result.setFont(font);
        result.setForeground(Color.BLACK);
        result.setEnabled(false);
        result.setDisabledTextColor(Color.BLACK);
        f.add(result);

        f.setSize(900, 600);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //read the transactions
        in = new Scanner(new File("pizza_orders.csv"));
        //ignore headers
        in.next();

        //container for all transaction
        ArrayList<Transaction> transactions = new ArrayList<>();

        temp = in.next();
        int lastOrder = Integer.valueOf(temp.substring(0, temp.indexOf(',')));
        String meal = temp.substring(temp.indexOf(',') + 1);

        Transaction t = new Transaction();
        t.addItem(Item.findByStringID(meal));

        while (in.hasNext()) {
            temp = in.next();
            //we are now in new order
            if (lastOrder != Integer.valueOf(temp.substring(0, temp.indexOf(',')))) {
                lastOrder = Integer.valueOf(temp.substring(0, temp.indexOf(',')));
                //this order has finished => add it to transaction array
                transactions.add(t);
                //define new order to hold its meals
                t = new Transaction();
            }
            //get the meal in this order
            meal = temp.substring(temp.indexOf(',') + 1);
            t.addItem(Item.findByStringID(meal));
        }
        //manually add the last transtion;
        transactions.add(t);

        //print #transactions
        System.out.println("#Transactions: "+transactions.size());

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //remove previuos results
                result.setText("");

                try {
                    int minSupport = Integer.valueOf(minSupportCount.getText());
                    double minConf = Double.valueOf(minConfidence.getText());
                    
                    if(minConf>100 || minConf<0)
                        throw new Exception("Minimum Confidence should be in range [0, 100]");
                    
                    if(minSupport<=0)
                        throw new Exception("Minimum Support should grater than 0");

                    //implement Apriori
                    Apriori ap = new Apriori(minSupport, transactions);

                    ap.extractRules(minConf, true);

                    for (Map.Entry<String, Double> rule : ap.rules.entrySet()) {
                        result.append("\n  " + rule.getKey() + ": " + String.format("%.2f", rule.getValue()));
                    }

                } catch (NumberFormatException nex){
                    result.setText("Minimun Support & Minimun Confidence should be numbers only...");
                } catch (Exception ex) {
                    result.setText(ex.getMessage());
                }

            }
        });
    }

}
